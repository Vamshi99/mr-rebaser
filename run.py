"""
This module contains the functions to run rebase and push operation on a git
repository.
"""
from enum import Enum
from shutil import rmtree
from subprocess import run
from subprocess import CalledProcessError
from subprocess import PIPE
from tempfile import mkdtemp
from typing import Optional
import json
import sys


class CommandStatus(Enum):
    # command executed successfully
    SUCCESS = 'success'
    # command failed with an error
    ERROR = 'error'


def run_command(command, cwd: str, stdin: (str, None)=None):
    """
    Runs a shell command and pipes the stdout and stderr streams.

    :raises CalledProcessError:
        If the shell command exits with a return code greater than zero.
    :return:
        Returns a tuple of piped stdout and stderr content.
    """
    command = command.strip().split()
    process = run(command, input=stdin, cwd=cwd, stdout=PIPE, stderr=PIPE)
    process.check_returncode()
    return (process.stdout.decode('utf-8'), process.stderr.decode('utf-8'))


def send_output(status: CommandStatus, error: Optional[CalledProcessError]):
    """
    Writes the output to standard output stream so that the calling process can
    use it.
    """
    output_dict = {'status': status.value}
    if error:
        output_dict['error'] = (f"Command: {' '.join(error.cmd)}\n\n"
                                f'Exit Code: {error.returncode}\n\n'
                                f'Cause:\n\n{error.stderr.decode()}\n')
    print(json.dumps(output_dict))


def _fetch(head_url, base_url, head_branch, base_branch):
    """
    Clones the git repository and fetches the head and base branches.
    """
    project_dir = mkdtemp()
    commands = (
        f'git clone --depth 1 {head_url} {project_dir}',
        f'git remote set-url origin {head_url}',
        f'git remote add upstream {base_url}',
        f'git config user.name GitMate',
        f'git config user.email info@gitmate.io',
        f'git fetch --depth 100 origin {head_branch}:head/{head_branch}',
        f'git checkout head/{head_branch}',
        f'git fetch --depth 100 upstream {base_branch}:base/{base_branch}',
    )
    for command in commands:
        run_command(command, cwd=project_dir)
    return project_dir


def merge(head_url, base_url, head_branch, base_branch, fast_forward=True):
    """
    Merges the head branch with base branch with specified merge strategy and
    pushes the changes onto the specified git repository.
    """
    try:
        project_dir = _fetch(head_url, base_url, head_branch, base_branch)
        merge_strategy = '--no-ff' if not fast_forward else '--ff-only'
        commands = [
            f'git checkout base/{base_branch}',
            f'git merge {merge_strategy} head/{head_branch}',
            f'git push upstream HEAD:{base_branch}'
        ]
        for command in commands:
            run_command(command, cwd=project_dir)
        status, error = CommandStatus.SUCCESS, None
    except CalledProcessError as ex:
        status, error = CommandStatus.ERROR, ex
    finally:
        rmtree(project_dir)
    return status, error


def rebase(head_url, base_url, head_branch, base_branch):
    """
    Rebases the head branch onto a source branch and pushes the changes onto
    the chosen git repository.
    """
    try:
        project_dir = _fetch(head_url, base_url, head_branch, base_branch)
        commands = (
            f'git rebase base/{base_branch}',
            f'git push --force origin HEAD:{head_branch}'
        )
        for command in commands:
            run_command(command, cwd=project_dir)
        status, error = CommandStatus.SUCCESS, None
    except CalledProcessError as ex:
        status, error = CommandStatus.ERROR, ex
    finally:
        rmtree(project_dir)
    return status, error


def main(args):
    """
    Executes the chosen git command on the repository.
    """
    if args[0].lower() == 'rebase':
        status, error = rebase(args[1], args[2], args[3], args[4])
    elif args[0].lower() in ['fastforward', 'ff']:
        status, error = merge(args[1], args[2], args[3], args[4])
    elif args[0].lower() == 'merge':
        status, error = merge(args[1], args[2], args[3], args[4], False)
    send_output(status, error)

if __name__ == '__main__':
    main(sys.argv[1:])
